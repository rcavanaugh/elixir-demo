defmodule Five do
	def greet(names) when is_binary(names) do
		names
			|> String.split(",")
			|> Enum.map(fn n -> String.trim(n) end)
			|> Enum.map(&String.capitalize/1)
			|> greet
	end

	def greet([name | rest]) do
		IO.puts "Hello #{name}"

		rest |> greet
	end

	def greet([]), do: :ok
end

names = "sandra, parth, nick"

Five.greet(names)
Five.greet(["mitch", "Peter"])