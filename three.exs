defmodule Three do
	def greet(person) when is_map(person) do
		"I don't know you, go away!"
	end

	def greet(%{first_name: first, last_name: last}) do
		"Hello #{first} #{last}"
	end

	def greet(%{first_name: first}) do
		"Hey #{first}"
	end
end

IO.puts Three.greet(%{first_name: "Rich", last_name: "Cavanaugh"})
IO.puts Three.greet(%{first_name: "Aaron"})
IO.puts Three.greet(%{})
IO.puts Three.greet(10)