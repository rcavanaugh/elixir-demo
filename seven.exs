defmodule Seven do
	def adder(state \\ 0)
	def adder(state) do
		receive do
			{:add, val} ->
				IO.puts "Adding #{val} to #{state}"
				adder(state + val)
			:stop ->
				IO.puts "Final value was #{state}"
		after
			1_000 -> 
				IO.puts "Quitting after one second of inactivity"
		end
	end
end

p = spawn(&Seven.adder/0)
Process.send(p, {:add, 10}, [])

Process.send(p, :stop, [])

p = spawn(&Seven.adder/0)
Process.send(p, {:add, 99}, [])

IO.inspect Process.alive?(p)

:timer.sleep(1_100)

IO.inspect Process.alive?(p)
Process.send(p, :stop, [])
