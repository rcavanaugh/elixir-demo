# elixir --erl "+P 2000000" --cookie nope --sname one eight.exs

defmodule Eight do
	def create_task(_) do
		Task.async(fn -> :timer.sleep(60_000) end)
	end

	def looper(i \\ 0)
	def looper(i) do
		IO.puts "Looping #{i}"
		:timer.sleep(2_000)
		looper(i + 1)
	end

	def tight_loop(i \\ 0)
	def tight_loop(_) do
		tight_loop(:rand.uniform(10))
	end
end

Process.spawn(&Eight.looper/0, [])
Process.spawn(&Eight.tight_loop/0, [])

:observer.start()

result = 1..1_000_000
	|> Enum.to_list
	|> Enum.map(&Eight.create_task/1)
	|> IO.inspect(label: "Created")
	|> Enum.map(&Task.await(&1, 120_000))
	|> IO.inspect(label: "Results")

IO.puts Enum.count(result)
