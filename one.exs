defmodule FizzBuzz do
	def fb(num) do 
		fb(num, rem(num, 3), rem(num, 5))
	end

	defp fb(n, 0, 0), do: "FIZZBUZZ"
	defp fb(_, 0, _), do: "FIZZ"
	defp fb(_, _, 0), do: "BUZZ"
	defp fb(n, _, _), do: n
end

IO.puts FizzBuzz.fb(1)
IO.puts FizzBuzz.fb(6)
IO.puts FizzBuzz.fb(10)
IO.puts FizzBuzz.fb(15)