defmodule Four do
	def greet(<<"Hi ", name::binary>>) do
		"Hello, #{name}."
	end

	def greet(<<"Welcome ", name::binary>>) do
		"Hello, #{name}."
	end
end

IO.puts Four.greet("Hi Rich")
IO.puts Four.greet("Welcome Adam")