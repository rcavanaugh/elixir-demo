defmodule Nine do
	###
	# Server
	###
	def divider() do
		receive do
			{:divide, from, ref, n1, n2} ->
				Process.send(from, {ref, n1 / n2}, [])
		end

		divider()
	end

	###
	# Client
	###
	def divide(proc, n1, n2) do
		ref = Process.monitor(proc)

		Process.send(proc, {:divide, self(), ref, n1, n2}, [])

		receive do
			{^ref, val} ->
				{:ok, val}
			{:DOWN, ^ref, _, _, reason} ->
				{:error, reason}
		end
	end
end

divider = spawn &Nine.divider/0

IO.inspect Nine.divide(divider, 10, 5)
IO.inspect Nine.divide(divider, 10, 0)