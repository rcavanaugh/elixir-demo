defmodule Two do
	def greet(person) do
		case person do
			%{first_name: first, last_name: last} ->
				"Hello #{first} #{last}"
			%{first_name: first} ->
				"Hey #{first}"
			person when is_map(person) ->
				"I don't know you, go away!"
		end
	end
end

IO.puts Two.greet(%{first_name: "Rich", last_name: "Cavanaugh"})
IO.puts Two.greet(%{first_name: "Aaron"})
IO.puts Two.greet(%{})
IO.puts Two.greet(10)
