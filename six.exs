defmodule Six do
	def loop(state \\ 0)
	def loop(state) do
		:timer.sleep(900)

		IO.puts state

		loop(state + 1)
	end
end

spawn &Six.loop/0

:timer.sleep(10_000)