defmodule Ten.Worker do
	use GenServer

	def start_link() do
		GenServer.start_link(__MODULE__, [], name: __MODULE__)
	end

	def init(_) do
		{:ok, 0}
	end

	def handle_call({:add, add}, _from, num) do
		new_num = num + add

		{:reply, {num, new_num}, new_num}
	end
end

defmodule Ten.Supervisor do
	use Supervisor

	def start_link do
		Supervisor.start_link(__MODULE__, [], [])
	end

	def init(_) do
		import Supervisor.Spec, warn: false

		children = [
			worker(Ten.Worker, [])
		]

		supervise(children, strategy: :one_for_one)
	end
end


{:ok, sup} = Ten.Supervisor.start_link



IO.puts "Do some addition with the adder by name"
# call the adder by name
IO.inspect GenServer.call(Ten.Worker, {:add, 10})



IO.puts "\nDo some addition with the adder pid"
# get the adder's pid by name
adder = Process.whereis(Ten.Worker)
IO.inspect(adder, label: "adder pid")

# call the adder by pid
IO.inspect GenServer.call(adder, {:add, 4})



IO.puts "\nKill the adder process"
# kill the adder
Process.exit(adder, :kill)

# check if the adder is alive
IO.inspect(Process.alive?(adder), label: "adder pid alive")



IO.puts "\nDo more addition with the adder by name"
# call the adder by name
IO.inspect GenServer.call(Ten.Worker, {:add, 10})



IO.puts "\nDo more addition with the adder pid"
# get the adder's pid by name
adder = Process.whereis(Ten.Worker)
IO.inspect(adder, label: "adder pid")

# check if the adder is alive
IO.inspect(Process.alive?(adder), label: "adder pid alive")



IO.puts "\nKill the supervisor"

# need this for the next bit to work because
# the supervisor is linked to us
Process.flag(:trap_exit, true)

# kill the supervisor
Process.exit(sup, :kill)

receive do
	{:EXIT, ^sup, reason} ->
		IO.puts "The supervisor exited for reason: #{reason}!"
end

# check if the adder is alive
IO.inspect(Process.alive?(adder), label: "adder pid alive")

IO.puts "\nTry to call the adder by name"

IO.inspect(self(), label: "my pid")

# call the adder by name
IO.inspect GenServer.call(Ten.Worker, {:add, 10})
